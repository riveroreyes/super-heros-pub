import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { LoaderService } from 'src/app/core/providers/loader/loader.service';
/**
 * @Description Contains the icon that is shown when a request is made to the server
 */
@Component({
  selector: 'app-loader',
  template: `
  casa
    <div class="loading-spinner" [hidden]="!loading">
      <h1>AQUI VA EL SPINNER</h1>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoaderComponent implements OnInit {
  loading = true;
  constructor(private loaderService: LoaderService, private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    console.log('loading'.toUpperCase(),this.loading);
    this.loaderService.isLoading.subscribe((v: any) => {
      this.loading = v;
      this.cd.detectChanges();
    });
  }
}
