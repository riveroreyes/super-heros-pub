import { Component } from '@angular/core';

/**
 * Application scaffold that contains the main menu and the body of each module
 */
@Component({
  selector: 'app-layout',
  templateUrl: './layout-app.component.html'
})
export class LayoutAppComponent {
  movilMode = false;

  constructor() {}

}
