import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { MainComponent } from './components/main/main.component';
import { Error404Component } from './components/error404/error404.component';
import { Error403Component } from './components/error403/error403.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { LayoutAppComponent } from './layout-app.component';

@NgModule({
  declarations: [LayoutAppComponent, MainComponent,  Error404Component, Error403Component, SideBarComponent],
  imports: [SharedModule]
})
export class LayoutModuleApp {}
