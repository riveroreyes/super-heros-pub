import { Component } from '@angular/core';

/**
 * 403 error view
 */
@Component({
  selector: 'app-error403',
  templateUrl: './error403.component.html'
})
export class Error403Component {
  constructor() {}
}
