import { Component } from '@angular/core';

/**
 * Main component of application scaffold
 */
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html'
})
export class MainComponent {}
