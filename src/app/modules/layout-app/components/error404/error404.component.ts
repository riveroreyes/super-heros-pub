import { Component } from '@angular/core';

/**
 * 404 error view
 */
@Component({
  selector: 'app-error404',
  templateUrl: './error404.component.html'
})
export class Error404Component {
  constructor() {}
}
