import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Error403Component } from './modules/layout-app/components/error403/error403.component';
import { Error404Component } from './modules/layout-app/components/error404/error404.component';
import { LayoutAppComponent } from './modules/layout-app/layout-app.component';

export const routes: Routes = [
  {
    path: '',
    component: LayoutAppComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
      }
    ],
    runGuardsAndResolvers: 'always'
  },
  { path: '403', component: Error403Component },
  { path: '**', component: Error404Component }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
