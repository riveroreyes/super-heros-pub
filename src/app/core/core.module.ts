import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoaderInterceptor } from './interceptors/loader-interceptor.service';
import { HttpErrorInterceptor } from './interceptors/http.error.interceptor';
import { LoaderService } from './providers/loader/loader.service';

const services: any[] = [
];

@NgModule({
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [
    ...services,
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    LoaderService
  ]
})
export class CoreModule {
  constructor() {
  }
}
